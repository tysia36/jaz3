/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: jetty/9.3.7.v20160115
 * Generated at: 2016-05-09 20:26:53 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class details_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

final java.lang.String _jspx_method = request.getMethod();
if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET POST or HEAD");
return;
}

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("    \r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<title>Insert title here</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      contracts.Contract contracts = null;
      synchronized (session) {
        contracts = (contracts.Contract) _jspx_page_context.getAttribute("contracts", javax.servlet.jsp.PageContext.SESSION_SCOPE);
        if (contracts == null){
          contracts = new contracts.Contract();
          _jspx_page_context.setAttribute("contracts", contracts, javax.servlet.jsp.PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("\r\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("contracts"), request);
      out.write("\r\n");
      out.write("\r\n");
      out.write("<h3>Wybrane dane dotyczące umowy:</h3>\r\n");
      out.write("\r\n");
	
	String workType;
	if(contracts.getContractType().equals("work_one"))
		workType="Umowa o Pracę";
	else if (contracts.getContractType().equals("work_two"))
		workType="Umowa o Dzieło";
	else
		workType="Umowa Zlecenie";
		
      out.write("\r\n");
      out.write("\r\n");
      out.write("Typ umowy: ");
      out.print(workType );
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<form action=\"numbers\" method=\"post\">\r\n");
 if(workType.equals("Umowa o Pracę"))
	{
      out.write("\r\n");
      out.write("\t<label> Miejsce  pracy<br />\r\n");
      out.write("\t <input type=\"radio\" name=\"workplace\" value=\"yes\" checked> Praca w miejscu zamieszkania <br />\r\n");
      out.write("\t  <input type=\"radio\" name=\"workplace\" value=\"no\"> Praca poza miejscem zamieszkania</label><br /><br />\r\n");
      out.write("\t");
}
	else if(workType.equals("Umowa Zlecenie"))
	{
	
      out.write("\r\n");
      out.write("<label> Składka rentowa<br />\r\n");
      out.write("<input type=\"radio\" name=\"rent\" value=\"yes\" checked> TAK\r\n");
      out.write(" <input type=\"radio\" name=\"rent\" value=\"no\"> NIE</label><br /><br />\r\n");
      out.write(" \r\n");
      out.write(" <label> Składka emerytalna<br />\r\n");
      out.write("<input type=\"radio\" name=\"pension\" value=\"yes\" checked> TAK\r\n");
      out.write(" <input type=\"radio\" name=\"pension\" value=\"no\"> NIE</label><br /><br />\r\n");
      out.write(" \r\n");
      out.write("  <label> Składka chorobowa<br />\r\n");
      out.write("<input type=\"radio\" name=\"disease\" value=\"yes\" checked> TAK\r\n");
      out.write(" <input type=\"radio\" name=\"disease\" value=\"no\"> NIE</label><br /><br />\r\n");
      out.write(" \r\n");
      out.write("   <label> Składka zdrowotna<br />\r\n");
      out.write("<input type=\"radio\" name=\"health\" value=\"yes\" checked> TAK\r\n");
      out.write(" <input type=\"radio\" name=\"health\" value=\"no\"> NIE</label><br /><br />\r\n");
      out.write(" \r\n");
      out.write("    <label> Koszty uzyskania przychodu<br />\r\n");
      out.write("<input type=\"radio\" name=\"workplace\" value=\"yes\" checked> 20% <br />\r\n");
      out.write(" <input type=\"radio\" name=\"workplace\" value=\"no\"> 50%</label><br /><br />\r\n");
      out.write(" \r\n");
      out.write(" ");
	}
	else
	{
      out.write("\r\n");
      out.write("\t\t <label> Koszty uzyskania przychodu<br />\r\n");
      out.write("\t\t <input type=\"radio\" name=\"workplace\" value=\"yes\" checked> 20% <br />\r\n");
      out.write("\t\t  <input type=\"radio\" name=\"workplace\" value=\"no\"> 50%</label><br /><br />\r\n");
      out.write("\t");
 }

      out.write("\r\n");
      out.write(" \r\n");
      out.write(" <input type=\"submit\" value=\"Dalej\">\r\n");
      out.write("</form>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}

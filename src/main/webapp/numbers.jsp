<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Liczby wynikowe</title>
</head>
<body>

<jsp:useBean id="contracts" class="contracts.Contract" scope="session"/>
<jsp:useBean id="detailss" class="contracts.Details" scope="session"/>
<jsp:useBean id="full" class="contracts.WorkOne" scope="session"/>
<jsp:useBean id="maybefull" class="contracts.WorkThree" scope="session"/>
<jsp:useBean id="notfull" class="contracts.WorkTwo" scope="session"/>

<jsp:setProperty property="*" name="detailss"/>

<h3>Wybrane dane dotyczące umowy:</h3>


<% 
	detailss.setContract(contracts);
	full.setWork(detailss);
	maybefull.setWork(detailss);
	notfull.setWork(detailss);
	String workType;
	if(contracts.getContractType().equals("work_one"))
		workType="Umowa o Pracę";
	else if (contracts.getContractType().equals("work_two"))
		workType="Umowa o Dzieło";
	else
		workType="Umowa Zlecenie";
	%>

Typ umowy: <%=workType %> <br />

<% if(workType.equals("Umowa o Pracę"))
	{
	full.Calculate();
%>
	Koszty utrzymania: <%=full.getMaintenanceCosts() %> <br />
	<br />
	<b>Brutto: <%=full.getAmountBrutto() %></b><br /> <br />
	Ubezpieczenia:<br />
	Zdrowotne: <%=full.getAmountHealth() %><br />
	Chorobowe: <%=full.getAmountDisease() %><br />
	Rentowe <%=full.getAmountRent() %><br />
	Emerytalne <%=full.getAmountPension() %><br />
	<br />
	Podatek: <%=full.getTax() %><br />
	Podstawa opodatkowania: <%=full.getTaxBase() %>
	<br />
	<br />
	<b>Netto: <%=full.getAmountNetto() %></b><br />
	
	<%}
	else if(workType.equals("Umowa Zlecenie"))
	{
	maybefull.Calculate();
%>
	Koszty utrzymania: <%=maybefull.getMaintenanceCosts() %> <br />
	<br />
	<b>Brutto: <%=maybefull.getAmountBrutto() %></b><br /> <br />
	Ubezpieczenia:<br />
	Zdrowotne: <%=maybefull.getAmountHealth() %><br />
	Chorobowe: <%=maybefull.getAmountDisease() %><br />
	Rentowe <%=maybefull.getAmountRent() %><br />
	Emerytalne <%=maybefull.getAmountPension() %><br />
	<br />
	Podatek: <%=maybefull.getTax() %><br />
	Podstawa opodatkowania: <%=maybefull.getTaxBase() %>
	<br />
	<br />
	<b>Netto: <%=maybefull.getAmountNetto() %></b><br />
	<%	}
	else
	{
		notfull.Calculate();
%>
	Koszty utrzymania: <%=notfull.getMaintenanceCosts() %> <br />
	<br />
	<b>Brutto: <%=notfull.getAmountBrutto() %></b><br /> <br />
	Ubezpieczenia:<br />
	Zdrowotne: <%=0 %><br />
	Chorobowe: <%=0 %><br />
	Rentowe <%=0 %><br />
	Emerytalne <%=0 %><br />
	<br />
	Podatek: <%=notfull.getTax() %><br />
	Podstawa opodatkowania: <%=notfull.getTaxBase() %>
	<br />
	<br />
	<b>Netto: <%=notfull.getAmountNetto() %></b><br />
	<%} %>
	
	



</body>
</html>
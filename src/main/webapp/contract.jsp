<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Rodzaj umowy</title>
</head>
<body>

<form action="details" method="post">
 <label> Wybierz rodzaj umowy: 
  <select name="contractType" id="contract">
    <option value="work_one">Umowa o Pracę</option>
    <option value="work_two">Umowa o Dzieło</option>
    <option value="work_three">Umowa Zlecenie</option>
  </select>
 </label> <br />
  <label>Wpisz rok wypłaty wynagrodzenia (2016-2099): <input type="number" min="2016" max="2099"  value="2016" 
  						name="contractYear" id="contractYear"></label><br />
 <label> Rodzaj wynagrodzenia<br />
<input type="radio" name="brutto" value="yes" checked> BRUTTO
 <input type="radio" name="brutto" value="no"> NETTO</label><br /><br />
  <label>Wpisz kwotę wynagrodzenia: <input type="number" name="amount" id="amount" step="0.01" required></label><br />
  <br><br>
  <input type="submit" value="Dalej">
</form>

</body>
</html>
package contracts;

public class WorkThree
{
	private Details work;
	
	private double amountBrutto;
	private double amountNetto;
	private double amountRent;			//rentowa
	private double amountPension;		//emerytalna
	private double amountDisease;		//chorobowa
	private double amountHealth;		//zdrowotna
	private double maintenanceCosts;	//koszty utrzymania
	private double taxBase;				//podstawa opodatkowania
	private double tax;					//podatek
	private double baseSalary;			//wynagrodzenie zasadnicze
	
	public double getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}
	
	public double getTaxBase() {
		return taxBase;
	}

	public void setTaxBase(double taxBase) {
		this.taxBase = taxBase;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public Details getWork() {
		return work;
	}

	public void setWork(Details work) {
		this.work = work;
	}

	public double getAmountBrutto() {
		return amountBrutto;
	}

	public void setAmountBrutto(double amountBrutto) {
		this.amountBrutto = amountBrutto;
	}

	public double getAmountNetto() {
		return amountNetto;
	}

	public void setAmountNetto(double amountNetto) {
		this.amountNetto = amountNetto;
	}

	public double getAmountRent() {
		return amountRent;
	}

	public void setAmountRent(double amountrent) {
		this.amountRent = amountrent;
	}

	public double getAmountPension() {
		return amountPension;
	}

	public void setAmountPension(double amountpension) {
		this.amountPension = amountpension;
	}

	public double getAmountDisease() {
		return amountDisease;
	}

	public void setAmountDisease(double amountdisease) {
		this.amountDisease = amountdisease;
	}

	public double getAmountHealth() {
		return amountHealth;
	}

	public void setAmountHealth(double amounthealth) {
		this.amountHealth = amounthealth;
	}

	public double getMaintenanceCosts() {
		return maintenanceCosts;
	}

	public void setMaintenanceCosts(double maintenanceCosts) {
		this.maintenanceCosts = maintenanceCosts;
	}
	
	public void Calculate()
	{
		if (this.getWork().getContract().getBrutto().equals("yes"))
		{
			this.setAmountBrutto(this.getWork().getContract().getAmount());
			if (this.getWork().getPension().equals("yes"))
				this.setAmountPension(this.Round(this.getAmountBrutto()*0.0976));
			else
				this.setAmountPension(0);
			if (this.getWork().getRent().equals("yes"))
				this.setAmountRent(this.Round(this.getAmountBrutto()*0.015));
			else
				this.setAmountRent(0);
			if (this.getWork().getDisease().equals("yes"))
				this.setAmountDisease(this.Round(this.getAmountBrutto()*0.0245));
			else
				this.setAmountDisease(0);
			this.setBaseSalary(this.Round(this.getAmountBrutto()-this.getAmountPension()-
					this.getAmountRent()-this.getAmountDisease()));
			
			if (this.getWork().getWorkplace().equals("yes"))
			{
				this.setMaintenanceCosts(this.Round(0.2*this.getBaseSalary()));
			}
			else
				this.setMaintenanceCosts(this.Round(0.5*this.getBaseSalary()));
			
			if (this.getBaseSalary()-this.getMaintenanceCosts()>0)
				this.setTaxBase(Math.round(this.getBaseSalary()-this.getMaintenanceCosts()));
			else
				this.setTaxBase(0);
			if(this.getTaxBase()>0)
			{
				this.setTax(this.Round(this.getTaxBase()*0.18));
				/*if (this.getTax()>46.33)
					this.setTax(this.Round(this.getTax()-46.33));
				else
					this.setTax(0);*/
			}
			else
				this.setTax(0);
			
			if (this.getWork().getHealth().equals("yes"))
				this.setAmountHealth(this.Round(this.getBaseSalary()*0.09));
			else
				this.setAmountHealth(0);
			if (this.getAmountHealth()>this.getTax())
				this.setAmountHealth(this.getTax());
			
			if (this.getWork().getHealth().equals("yes"))
			{
				if ((this.getBaseSalary()*0.0775)<this.getTax())
					this.setTax(Math.round(this.getTax()-(this.getBaseSalary()*0.0775)));
				else
					this.setTax(0);
			}
			
			this.setAmountNetto(this.Round(this.getBaseSalary()-this.getAmountHealth()-this.getTax()));			
			
		}
		else
		{
			this.setAmountNetto(this.getWork().getContract().getAmount());
			double factor=0.01;
			double brutto=this.getAmountNetto();
			double netto=this.calculateNetto(brutto);
			while(netto!=this.getAmountNetto())
			{
				brutto=this.Round(brutto+factor);
				netto=this.calculateNetto(brutto);
			}
			this.setAmountBrutto(brutto);
			
			if (this.getWork().getPension().equals("yes"))
				this.setAmountPension(this.Round(this.getAmountBrutto()*0.0976));
			else
				this.setAmountPension(0);
			if (this.getWork().getRent().equals("yes"))
				this.setAmountRent(this.Round(this.getAmountBrutto()*0.015));
			else
				this.setAmountRent(0);
			if (this.getWork().getDisease().equals("yes"))
				this.setAmountDisease(this.Round(this.getAmountBrutto()*0.0245));
			else
				this.setAmountDisease(0);
			this.setBaseSalary(this.Round(this.getAmountBrutto()-this.getAmountPension()-
					this.getAmountRent()-this.getAmountDisease()));
			
			if (this.getWork().getWorkplace().equals("yes"))
			{
				this.setMaintenanceCosts(this.Round(0.2*this.getBaseSalary()));
			}
			else
				this.setMaintenanceCosts(this.Round(0.5*this.getBaseSalary()));
			
			if (this.getBaseSalary()-this.getMaintenanceCosts()>0)
				this.setTaxBase(Math.round(this.getBaseSalary()-this.getMaintenanceCosts()));
			else
				this.setTaxBase(0);
			if(this.getTaxBase()>0)
			{
				this.setTax(this.Round(this.getTaxBase()*0.18));
				/*if (this.getTax()>46.33)
					this.setTax(this.Round(this.getTax()-46.33));
				else
					this.setTax(0);*/
			}
			else
				this.setTax(0);
			
			if (this.getWork().getHealth().equals("yes"))
				this.setAmountHealth(this.Round(this.getBaseSalary()*0.09));
			else
				this.setAmountHealth(0);
			if (this.getAmountHealth()>this.getTax())
				this.setAmountHealth(this.getTax());
			
			if (this.getWork().getHealth().equals("yes"))
			{
				if ((this.getBaseSalary()*0.0775)<this.getTax())
					this.setTax(Math.round(this.getTax()-(this.getBaseSalary()*0.0775)));
				else
					this.setTax(0);
			}
			
		}
	}
	
	public double Round(double number)
	{
		double numberRounded;
		numberRounded=number*100;
		numberRounded=Math.round(numberRounded);
		numberRounded=numberRounded/100;
		return numberRounded;
	}
	
	public double calculateNetto(double brutto)
	{
		double baseSalary=brutto;
		double taxBase;
		double maintenance;
		
		if (this.getWork().getPension().equals("yes"))
			baseSalary=baseSalary-this.Round(brutto*0.0976);
		if (this.getWork().getRent().equals("yes"))
			baseSalary=baseSalary-this.Round(brutto*0.015);
		if (this.getWork().getDisease().equals("yes"))
			baseSalary=baseSalary-this.Round(brutto*0.0245);
		
		baseSalary=this.Round(baseSalary);
		
		if (this.getWork().getWorkplace().equals("yes"))
		{
			maintenance=this.Round(0.2*baseSalary);
		}
		else
			maintenance=this.Round(0.5*baseSalary);
		if (baseSalary-maintenance>0)
			taxBase=Math.round(baseSalary-maintenance);
		else
			taxBase=0;
		double tax;
		if(taxBase>0)
		{
			tax=this.Round(taxBase*0.18);
			/*
			if (tax>46.33)
				tax=this.Round(tax-46.33);
			else
				tax=0;*/
		}
		else
			tax=0;
		
		double health=0;
		if (this.getWork().getHealth().equals("yes"))
		{
			health=this.Round(baseSalary*0.09);
			if (health>tax)
				health=tax;
			if ((baseSalary*0.0775)<tax)
				tax=Math.round(tax-(baseSalary*0.0775));
			else
				tax=0;
		}
		
		double netto=this.Round(baseSalary-health-tax);
		
		return netto;
	}

	

}

package contracts;

public class Contract 
{
	
	private String contractType;
	private int contractYear;
	private String brutto;
	private double amount;
	
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public int getContractYear() {
		return contractYear;
	}
	public void setContractYear(int contractYear) {
		this.contractYear = contractYear;
	}
	public String getBrutto() {
		return brutto;
	}
	public void setBrutto(String brutto) {
		this.brutto = brutto;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

}

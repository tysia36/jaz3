package contracts;

public class WorkTwo
{
	private Details work;
	
	private double amountBrutto;
	private double amountNetto;
	private double maintenanceCosts;	//koszty utrzymania
	private double taxBase;				//podstawa opodatkowania
	private double tax;					//podatek
	
	public double getTaxBase() {
		return taxBase;
	}

	public void setTaxBase(double taxBase) {
		this.taxBase = taxBase;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public Details getWork() {
		return work;
	}

	public void setWork(Details work) {
		this.work = work;
	}

	public double getAmountBrutto() {
		return amountBrutto;
	}

	public void setAmountBrutto(double amountBrutto) {
		this.amountBrutto = amountBrutto;
	}

	public double getAmountNetto() {
		return amountNetto;
	}

	public void setAmountNetto(double amountNetto) {
		this.amountNetto = amountNetto;
	}

	public double getMaintenanceCosts() {
		return maintenanceCosts;
	}

	public void setMaintenanceCosts(double maintenanceCosts) {
		this.maintenanceCosts = maintenanceCosts;
	}
	
	public void Calculate()
	{
		if (this.getWork().getContract().getBrutto().equals("yes"))
		{
			this.setAmountBrutto(this.getWork().getContract().getAmount());
			
			if (this.getWork().getWorkplace().equals("yes"))
			{
				this.setMaintenanceCosts(this.Round(0.2*this.getAmountBrutto()));
			}
			else
				this.setMaintenanceCosts(this.Round(0.5*this.getAmountBrutto()));
			
			if (this.getAmountBrutto()-this.getMaintenanceCosts()>0)
				this.setTaxBase(Math.round(this.getAmountBrutto()-this.getMaintenanceCosts()));
			else
				this.setTaxBase(0);
			if(this.getTaxBase()>0)
			{
				this.setTax(this.Round(this.getTaxBase()*0.18));
				/*if (this.getTax()>46.33)
					this.setTax(this.Round(this.getTax()-46.33));
				else
					this.setTax(0);*/
			}
			else
				this.setTax(0);
			
			
			this.setAmountNetto(this.Round(this.getAmountBrutto()-this.getTax()));			
			
		}
		else
		{
			this.setAmountNetto(this.getWork().getContract().getAmount());
			double factor=0.01;
			double brutto=this.getAmountNetto();
			double netto=this.calculateNetto(brutto);
			while(netto!=this.getAmountNetto())
			{
				brutto=this.Round(brutto+factor);
				netto=this.calculateNetto(brutto);
			}
			this.setAmountBrutto(brutto);
			
			if (this.getWork().getWorkplace().equals("yes"))
			{
				this.setMaintenanceCosts(this.Round(0.2*this.getAmountBrutto()));
			}
			else
				this.setMaintenanceCosts(this.Round(0.5*this.getAmountBrutto()));
			
			if (this.getAmountBrutto()-this.getMaintenanceCosts()>0)
				this.setTaxBase(Math.round(this.getAmountBrutto()-this.getMaintenanceCosts()));
			else
				this.setTaxBase(0);
			if(this.getTaxBase()>0)
			{
				this.setTax(this.Round(this.getTaxBase()*0.18));
				/*if (this.getTax()>46.33)
					this.setTax(this.Round(this.getTax()-46.33));
				else
					this.setTax(0);*/
			}
			else
				this.setTax(0);
			
		}
	}
	
	public double Round(double number)
	{
		double numberRounded;
		numberRounded=number*100;
		numberRounded=Math.round(numberRounded);
		numberRounded=numberRounded/100;
		return numberRounded;
	}
	
	public double calculateNetto(double brutto)
	{
		double taxBase;
		double maintenance;
		
		if (this.getWork().getWorkplace().equals("yes"))
		{
			maintenance=this.Round(0.2*brutto);
		}
		else
			maintenance=this.Round(0.5*brutto);
		if (brutto-maintenance>0)
			taxBase=Math.round(brutto-maintenance);
		else
			taxBase=0;
		double tax;
		if(taxBase>0)
		{
			tax=this.Round(taxBase*0.18);
			/*
			if (tax>46.33)
				tax=this.Round(tax-46.33);
			else
				tax=0;*/
		}
		else
			tax=0;
		
		double netto=this.Round(brutto-tax);
		
		return netto;
	}

	

}
package contracts;

public class WorkOne 
{
	private Details work;
	
	private double amountBrutto;
	private double amountNetto;
	private double amountRent;			//rentowa
	private double amountPension;		//emerytalna
	private double amountDisease;		//chorobowa
	private double amountHealth;		//zdrowotna
	private double maintenanceCosts;	//koszty utrzymania
	private double taxBase;				//podstawa opodatkowania
	private double tax;					//podatek
	private double baseSalary;			//wynagrodzenie zasadnicze
	
	public double getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}
	
	public double getTaxBase() {
		return taxBase;
	}

	public void setTaxBase(double taxBase) {
		this.taxBase = taxBase;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public Details getWork() {
		return work;
	}

	public void setWork(Details work) {
		this.work = work;
	}

	public double getAmountBrutto() {
		return amountBrutto;
	}

	public void setAmountBrutto(double amountBrutto) {
		this.amountBrutto = amountBrutto;
	}

	public double getAmountNetto() {
		return amountNetto;
	}

	public void setAmountNetto(double amountNetto) {
		this.amountNetto = amountNetto;
	}

	public double getAmountRent() {
		return amountRent;
	}

	public void setAmountRent(double amountrent) {
		this.amountRent = amountrent;
	}

	public double getAmountPension() {
		return amountPension;
	}

	public void setAmountPension(double amountpension) {
		this.amountPension = amountpension;
	}

	public double getAmountDisease() {
		return amountDisease;
	}

	public void setAmountDisease(double amountdisease) {
		this.amountDisease = amountdisease;
	}

	public double getAmountHealth() {
		return amountHealth;
	}

	public void setAmountHealth(double amounthealth) {
		this.amountHealth = amounthealth;
	}

	public double getMaintenanceCosts() {
		return maintenanceCosts;
	}

	public void setMaintenanceCosts(double maintenanceCosts) {
		this.maintenanceCosts = maintenanceCosts;
	}
	
	public void Calculate()
	{
		if (this.getWork().getWorkplace().equals("yes"))
		{
			this.setMaintenanceCosts(111.25);
		}
		else
			this.setMaintenanceCosts(139.06);
		if (this.getWork().getContract().getBrutto().equals("yes"))
		{
			this.setAmountBrutto(this.getWork().getContract().getAmount());
			this.setAmountPension(this.Round(this.getAmountBrutto()*0.0976));
			this.setAmountRent(this.Round(this.getAmountBrutto()*0.015));
			this.setAmountDisease(this.Round(this.getAmountBrutto()*0.0245));
			this.setBaseSalary(this.Round(this.getAmountBrutto()-this.getAmountPension()-
					this.getAmountRent()-this.getAmountDisease()));
			
			if (this.getBaseSalary()-this.getMaintenanceCosts()>0)
				this.setTaxBase(Math.round(this.getBaseSalary()-this.getMaintenanceCosts()));
			else
				this.setTaxBase(0);
			if(this.getTaxBase()>0)
			{
				this.setTax(this.Round(this.getTaxBase()*0.18));
				if (this.getTax()>46.33)
					this.setTax(this.Round(this.getTax()-46.33));
				else
					this.setTax(0);
			}
			else
				this.setTax(0);
			
			this.setAmountHealth(this.Round(this.getBaseSalary()*0.09));
			if (this.getAmountHealth()>this.getTax())
				this.setAmountHealth(this.getTax());
			
			if ((this.getBaseSalary()*0.0775)<this.getTax())
				this.setTax(Math.round(this.getTax()-(this.getBaseSalary()*0.0775)));
			else
				this.setTax(0);
			
			this.setAmountNetto(this.Round(this.getBaseSalary()-this.getAmountHealth()-this.getTax()));			
			
		}
		else
		{
			this.setAmountNetto(this.getWork().getContract().getAmount());
			double factor=0.01;
			double brutto=this.getAmountNetto();
			double netto=this.calculateNetto(brutto);
			while(netto!=this.getAmountNetto())
			{
				brutto=this.Round(brutto+factor);
				netto=this.calculateNetto(brutto);
			}
			this.setAmountBrutto(brutto);
			this.setAmountPension(this.Round(this.getAmountBrutto()*0.0976));
			this.setAmountRent(this.Round(this.getAmountBrutto()*0.015));
			this.setAmountDisease(this.Round(this.getAmountBrutto()*0.0245));
			this.setBaseSalary(this.Round(this.getAmountBrutto()-this.getAmountPension()-
					this.getAmountRent()-this.getAmountDisease()));
			
			if (this.getBaseSalary()-this.getMaintenanceCosts()>0)
				this.setTaxBase(Math.round(this.getBaseSalary()-this.getMaintenanceCosts()));
			else
				this.setTaxBase(0);
			if(this.getTaxBase()>0)
			{
				this.setTax(this.Round(this.getTaxBase()*0.18));
				if (this.getTax()>46.33)
					this.setTax(this.Round(this.getTax()-46.33));
				else
					this.setTax(0);
			}
			else
				this.setTax(0);
			
			this.setAmountHealth(this.Round(this.getBaseSalary()*0.09));
			if (this.getAmountHealth()>this.getTax())
				this.setAmountHealth(this.getTax());
			
			if ((this.getBaseSalary()*0.0775)<this.getTax())
				this.setTax(Math.round(this.getTax()-(this.getBaseSalary()*0.0775)));
			else
				this.setTax(0);
		}
	}
	
	public double Round(double number)
	{
		double numberRounded;
		numberRounded=number*100;
		numberRounded=Math.round(numberRounded);
		numberRounded=numberRounded/100;
		return numberRounded;
	}
	
	public double calculateNetto(double brutto)
	{
		double baseSalary=this.Round(brutto-this.Round(brutto*0.0976)-this.Round(brutto*0.015)-this.Round(brutto*0.0245));
		double taxBase;
		
		if (baseSalary-this.getMaintenanceCosts()>0)
			taxBase=Math.round(baseSalary-this.getMaintenanceCosts());
		else
			taxBase=0;
		double tax;
		if(taxBase>0)
		{
			tax=this.Round(taxBase*0.18);
			if (tax>46.33)
				tax=this.Round(tax-46.33);
			else
				tax=0;
		}
		else
			tax=0;
	
		double health=this.Round(baseSalary*0.09);
		if (health>tax)
			health=tax;
		if ((baseSalary*0.0775)<tax)
			tax=Math.round(tax-(baseSalary*0.0775));
		else
			tax=0;
		
		double netto=this.Round(baseSalary-health-tax);
		
		return netto;
	}

	

}
